import tkinter as tk
import subprocess
from tkinter.constants import LEFT, YES
from check import *

frame = tk.Tk()
frame.title("Input Code Program Mahasiswa")
frame.geometry('600x600')

bahasa=int

contohpython = "python/answer.py"
contohjava = "java/answer.java"

def inputpython():
	inp = inputtxt.get(1.0, "end-1c")
	tulis_coding = open(contohpython, "w")
	# tulis teks ke file
	tulis_coding.write(inp)
	# tutup file
	tulis_coding.close()
	runcheck = "Sukses Input"
	lbl.config(text = runcheck)
	
def inputjava():
	inp = inputtxt.get(1.0, "end-1c")
	tulis_coding = open(contohjava, "w")
	# tulis teks ke file
	tulis_coding.write(inp)
	# tutup file
	tulis_coding.close()
	runcheck = "Sukses Input"
	lbl.config(text = runcheck)


# TextBox Creation
inputtxt = tk.Text(frame,
				height = 20,
				width = 80)

inputtxt.pack()

# Button Creation
buttonpython = tk.Button(frame,
						text = "Submit Coding python",
						command = inputpython)
buttonjava = tk.Button(frame, text="Submit Coding Java", command=inputjava)

buttonjava.pack(side=LEFT,expand=YES)
buttonpython.pack(side=LEFT,expand=YES)


# Label Creation
lbl = tk.Label(frame, text = "")
lbl.pack()
frame.mainloop()
