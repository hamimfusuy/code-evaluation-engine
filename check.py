import subprocess

def runpython():	
        try:

                contoh='python3 python/answer1.py'
                test = 'python3 python/key1.py'
                runcontoh = subprocess.check_output(contoh, shell=True)
                outputcontoh = runcontoh
                runtest = subprocess.check_output(test, shell=True)
                outputtest = runtest

                if(outputcontoh == outputtest):
                        hasil='benar'
                else:
                        hasil='salah'

                print("Hasil Program = ",hasil)

        except (subprocess.CalledProcessError, NameError) as e:
                print("Program Mahasiswa Error")

def testcasepython():
        try:
                tc_answer = 'python -u python/testcase.py'
                tc_key = 'python -u python/keytc.py'


                answ_tc = subprocess.check_output(tc_answer, shell=True)
                key_tc  = subprocess.check_output(tc_key, shell=True)
                out_answ = answ_tc
                out_key = key_tc

                if out_answ == out_key:
                        print("TestCase berhasil")
                else:
                        print("gagal")
        except (subprocess.CalledProcessError, NameError) as e:
                print("Program Mahasiswa Error")

def runjava():
        
        contoh='java java/answer.java'
        test = 'java java/key.java'
        try:
                runcontoh = subprocess.check_output(contoh, shell=True)
                outputcontoh = runcontoh
                runtest = subprocess.check_output(test, shell=True)
                outputtest = runtest

                
                                
                if(outputcontoh==outputtest):
                        hasil='benar'
                else:
                        hasil='salah'

                if(hasil=='salah'):
                        print("hasil program ",hasil)
                else:
                        print("hasil progam ",hasil)

        except (subprocess.CalledProcessError, NameError) as e:
                print("Program Mahasiswa Error")


print("###########################################\n")
print("##### Program Code Evaluation Engine ######\n")
print("###########################################\n")
print("##### 1. Cek program Python          ######\n")
print("##### 2. Cek program Java            ######\n")
print("###########################################\n")
print("##### Pilihan = ") 
pilih=int(input())
if pilih==1:
        print("##### Apakah program python memiliki Testcase (y/t) = ")
        y=input()
        if y=='y' or y=='Y':
                testcasepython()
        elif y=='t' or y=='T':
                runpython()
        else:
                print("pilihan salah")
elif pilih==2:
        runjava()
else:
        print("#####  Pilihan Salah          ######\n")







